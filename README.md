*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# summary

This is a `git` repository for the code for [ICON][ICON overview @ CMAS wiki], constructed from the `cvs`-based [tarballs][CMAQ-5.0.1 download page] distributed for [CMAQ version=5.0.1][CMAQ-5.0.1 @ CMAS wiki]. It was built using [CMAQ-build][CMAQ-build/one_way_tarsplat_to_git.sh @ bitbucket].

[ICON overview @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Initial_Conditions_Processor_.28ICON.29
[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[CMAQ-5.0.1 download page]: https://www.cmascenter.org/download/software/cmaq/cmaq_5-0-1.cfm
[CMAQ-5.0.1 @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD
[CMAQ-build/one_way_tarsplat_to_git.sh @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/src/HEAD/one_way_tarsplat_to_git.sh?at=master
